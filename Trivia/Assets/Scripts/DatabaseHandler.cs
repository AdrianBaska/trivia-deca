﻿using UnityEngine;
using Mono.Data.Sqlite;
using System.IO;

using System;

using System.Data;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;



//namespace NPOI.Examples.XSSF.SetCellValuesInXlsx;

public class DatabaseHandler : MonoBehaviour
{
    SqliteConnection conn;

    void Start()
    {
        if (!File.Exists("Database.sqlite"))
        {
            SqliteConnection.CreateFile("Database.sqlite");

        }
        conn = new SqliteConnection("Data Source=Database.sqlite;Version=3;");
        conn.Open();
        // probar qu eesta bien y cerrar
        conn.Close();


        //string[] col = { "id", "name" , "score"};
        //string[] type = { "INTEGER PRIMARY KEY", "TEXT", "TEXT" };

       // string name = "name, last";
 

        //CreateTable("db",col,type);
        //ExportExcel();
        //DeleteTableContents("leaderboard");
        //AddColumn("prueba", "last","TEXT");
        //InsertSingleRec("db", "name", "Jose");
        //InsertSingleRec("db", "name", "Fred");
        //UpdateSingle("db", "score = '400'", "name = 'Fred'");
        //UPDATE company SET address = 'Texas' WHERE id = 6;
        //InsertSingleRec("leaderboard", "name", "Adrian");
        //SelectRecOrderBy("db", "name, score", "name", 5);
        //InsertSingle("prueba2", "name", "fredo");
        //InsertMultiple("prueba2", name,dato);
        //DeleteSingleRecord("prueba", "Name","fredo");
        
        //RetrieveRecord("prueba", name);


    }

    public void CreateTable(string name, string[] column, string[] type)
    {
        conn.Open();
        SqliteCommand cmd = new SqliteCommand(conn);
        
        //cmd.CommandText = "CREATE TABLE" + name + "(Id INTEGER PRIMARY KEY, Name TEXT, Price INT)";

        cmd.CommandText = "CREATE TABLE iF NOT EXISTS " + name + "(" + column[0] + " " + type[0];
        for (var i = 1; i < column.Length; i++)
        {
            cmd.CommandText += ", " + column[i] + " " + type[i];
        }
        cmd.CommandText += ")";

        cmd.ExecuteNonQuery();
        conn.Close();

        


        //using (SqliteConnection con = new SqliteConnection { ConnectionString = "Data Source=database.sqlite;Version=3;" })           
        //{
        //    using (SqliteCommand cmd = new SqliteCommand { Connection = con })
        //    {
        //        // check connection state if open then close, else close proceed
        //        if (con.State == ConnectionState.Open)
        //            con.Close();

        //        //then
        //        try
        //        {
        //            // try connection to Open
        //            con.Open();
        //        }
        //        catch
        //        {
        //            // catch connection error.
        //        }

        //        try
        //        {
        //            cmd.CommandText = "INSERT INTO " + table + " (" + column[0];
        //            for (var i = 1; i < column.Length; i++)
        //            {
        //                cmd.CommandText += ", " + column[i];
        //            }
        //            cmd.CommandText += ") VALUES(" + data[0];
        //            for (var i = 1; i < data.Length; i++)
        //            {
        //                cmd.CommandText += ", " + data[i];
        //            }
        //            cmd.CommandText += ")";
        //            Debug.Log(cmd.CommandText);// your insert query
        //        }
        //        catch
        //        {
        //            // catch query error
        //        }

        //    } // Close Command

        //}  // Close Connection

    }
    private bool TableExists(String tableName)
    {  
        conn.Open();
       // SqliteCommand cmd = new SqliteCommand(conn);
        if (tableName == null )
        {
            return false;
        }
        //cmd.CommandText = "SELECT count(*) FROM sqlite_master WHERE type = 'table' AND name = '" + name + "'";

        return  false;
    }
    public void InsertSingleRec(string table, string column, string dato)
    {
        conn.Open();
        SqliteCommand cmd = new SqliteCommand(conn);

        cmd.CommandText = "INSERT INTO " + table + " (" + column +") VALUES(@Dato)";
        cmd.Prepare();

        cmd.Parameters.AddWithValue("@Dato", dato);
        cmd.ExecuteNonQuery();
        conn.Close();
    }
    public void InsertSingleNoRepeat(string table, string column, string dato)
    {   if (dato != ""){
            string check = SelectSingleSTR(table, column, "name = '" + dato + "'");
            //Debug.Log("Result es: "+ check);
            if (check == "")
            {

                conn.Open();
                SqliteCommand cmd = new SqliteCommand(conn);

                cmd.CommandText = "INSERT INTO " + table + " (" + column + ") VALUES(@Dato)";
                Debug.Log("Nuevo jugador guardado");
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@Dato", dato);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
        else
        {
            Debug.Log("Espacio en blanco");
        }
        
    }
    // Insert multiple data at once
    public void InsertMultiple(string table, string[] column, string[] data)
    {
        using (SqliteConnection con = new SqliteConnection { ConnectionString = "Data Source=database.sqlite;Version=3;" })
        {
            using (SqliteCommand cmd = new SqliteCommand { Connection = con })
            {
                if (con.State == ConnectionState.Open)
                    con.Close();

                try
                {
                    con.Open();
                }
                catch
                {
                    // catch connection error
                }

                try
                {
                    cmd.CommandText = "INSERT INTO " + table + " (" + column[0];
                    for (var i = 1; i < column.Length; i++)
                    {
                        cmd.CommandText += ", " + column[i];
                    }
                    cmd.CommandText += ") VALUES(" + data[0];
                    for (var i = 1; i < data.Length; i++)
                    {
                        cmd.CommandText += ", " + data[i];
                    }
                    cmd.CommandText += ")";
                    Debug.Log(cmd.CommandText);
                    cmd.ExecuteNonQuery();
                }
                catch
                {
                    // catch query error
                }

            } 

        }

    }


    //Deletes a single record from a table
    //
    //Params: table name, column name and who to delete
    //
    public void DeleteSingleRecord(string table, string column, string who)
    {
        conn.Open();
        SqliteCommand cmd = new SqliteCommand(conn);

        cmd.CommandText = "DELETE FROM " + table + " WHERE " + column + " = '" + who+"'";

        cmd.ExecuteNonQuery();
        conn.Close();
    }


    //Deletes all data from specific table
    //
    //Params: table name
    //
    //
    public void DeleteTableContents(string tabla)
    {
        conn.Open();
        SqliteCommand cmd = new SqliteCommand(conn);
      
        cmd.CommandText = "DELETE FROM " + tabla;

        cmd.ExecuteNonQuery();
        conn.Close();
    }
    // under construction
    public void RetrieveRecord(string table, string what, string who = "none", string orderBy = "none", int limit = 0)
    {
        using (SqliteConnection c = new SqliteConnection("Data Source=database.sqlite;Version=3;"))
        {
            c.Open();
            using (SqliteCommand cmd = new SqliteCommand(c))
            {
                cmd.CommandText = "SELECT name, last FROM prueba2";
                using (SqliteDataReader rdr = cmd.ExecuteReader())
                {
                    //cmd.CommandText = "SELECT " + what + " FROM " + table;
                    int counter = 1;
                    //Debug.Log(rdr.FieldCount);
                    while (rdr.Read())

                    {
                        string firstName = rdr.GetValue(0).ToString();
                        string lasts = rdr.GetValue(1).ToString();

                        //Debug.Log(counter);
                        Debug.Log("Nombre" + firstName);
                        Debug.Log("apellido" + lasts);
                        counter++;
                    }
                }
            }
        }
    }
    // restrieves all data from table
    public void SelectAll(string table)
    {
        using (SqliteConnection c = new SqliteConnection("Data Source=database.sqlite;Version=3;"))
        {
            c.Open();
            using (SqliteCommand cmd = new SqliteCommand(c))
            {
                cmd.CommandText = "SELECT * FROM " + table;
                using (SqliteDataReader rdr = cmd.ExecuteReader())
                {
                    //cmd.CommandText = "SELECT " + what + " FROM " + table;
                    int counter = 1;
                    //Debug.Log(rdr.FieldCount);
                    while (rdr.Read())

                    {
                        string firstName = rdr.GetValue(1).ToString();
                        string lasts = rdr.GetValue(2).ToString();
                        string score = rdr.GetValue(3).ToString();
                        
                        Debug.Log("Nombre: " + firstName + " Email: " + lasts + " Score: " + score);
                        counter++;
                    }
                }
            }
        }
    }
    //retrieve data ordered by a variable in asc or desc order with limit
    public List<Dictionary<string, string>> SelectOrderBy(string table, string what, string orderBy, int limit)
    {
        List<Dictionary<string, string>> dictList = new List<Dictionary<string, string>>();
        using (SqliteConnection c = new SqliteConnection("Data Source=database.sqlite;Version=3;"))
        {
            c.Open();
            using (SqliteCommand cmd = new SqliteCommand(c))
            {
                cmd.CommandText = "SELECT " + what + " FROM " + table + " ORDER BY " + orderBy + " LIMIT " + limit;
                //Debug.Log(cmd.CommandText);
                using (SqliteDataReader rdr = cmd.ExecuteReader())
                {
                    //cmd.CommandText = "SELECT " + what + " FROM " + table;
                    int counter = 1;
                    //Debug.Log(rdr.FieldCount);
                    while (rdr.Read())

                    {
                        string name = rdr.GetValue(0).ToString();
                        string score = rdr.GetValue(1).ToString();

                        //Debug.Log(counter);
                        //Debug.Log("Nombre: " + name + " Score: " + score);                    

                        // declaration:
                        
                        Dictionary<string, string> dict = new Dictionary<string, string>();

                        dict["name"] = name;                 // insert or change the value for the given key
                        dict["score"] = score;

                        //ValueType thisValue = myDictionary[theKey];      // retrieve a value for the given key
                        //int howBig = myDictionary.Count;                 // get the number of items in the Hashtable
                        //myDictionary.Remove(theKey);                     // remove the key & value pair from the Hashtable, for the given key.
                        //Debug.Log(dict["score"]);
                        dictList.Add(dict);
                        //Debug.Log("llegue aqui");
                        counter++;
                    }
                }
            }
        } return dictList;
    }
    //return first in the list as a string
    public string Selectfirst(string table, string what, string orderBy, int limit)
    {
        string result = "";
        using (SqliteConnection c = new SqliteConnection("Data Source=database.sqlite;Version=3;"))
        {
            c.Open();
            using (SqliteCommand cmd = new SqliteCommand(c))
            {
                cmd.CommandText = "SELECT " + what + " FROM " + table + " ORDER BY " + orderBy + " LIMIT " + limit;
                //Debug.Log(cmd.CommandText);
                using (SqliteDataReader rdr = cmd.ExecuteReader())
                {
                    //cmd.CommandText = "SELECT " + what + " FROM " + table;
                    int counter = 1;
                    //Debug.Log(rdr.FieldCount);
                    while (rdr.Read())

                    {
                        string firstName = rdr.GetValue(0).ToString();
                        string lasts = rdr.GetValue(1).ToString();

                        //Debug.Log(counter);
                        result = "Nombre: " + firstName + " Score: " + lasts;
                        counter++;
                        return result;
                    }
                }
            }
        }
        return result;
    }
    // Select record that match a name
    // SELECT name FROM table WHERE name = paco
    public int SelectSingleINT(string table, string what, string how)
    {
        int result=-1;
        using (SqliteConnection c = new SqliteConnection("Data Source=database.sqlite;Version=3;"))
        {
            c.Open();
            using (SqliteCommand cmd = new SqliteCommand(c))
            {
                cmd.CommandText = "SELECT " + what + " FROM " + table + " WHERE " + how ;
                //Debug.Log(cmd.CommandText);
                using (SqliteDataReader rdr = cmd.ExecuteReader())
                {
                    //cmd.CommandText = "SELECT " + what + " FROM " + table;
                    //Debug.Log("Cuenta es"+rdr.FieldCount);
                    while (rdr.Read())
                    {
                        //Debug.Log("estoy aqui");
                        //Debug.Log(counter);
                        //Debug.Log(rdr.GetValue(0).GetType());
                        //if(rdr.GetDataTypeName(0) == "INTEGER")
                        //{
                        //    Debug.Log("problemo");
                        //    break;
                        //}
                        string msj = rdr.GetValue(0).ToString();
                        //Debug.Log("Mensaje es: "+msj);
                        result = int.Parse(msj);
                        //Debug.Log("Result es: "+result);
                    }
                }
            }
        }
        return result;
    }
    public string SelectSingleSTR(string table, string what, string how)
    {
        string result = "";
        using (SqliteConnection c = new SqliteConnection("Data Source=database.sqlite;Version=3;"))
        {
            c.Open();
            using (SqliteCommand cmd = new SqliteCommand(c))
            {
                cmd.CommandText = "SELECT " + what + " FROM " + table + " WHERE " + how;
                //Debug.Log(cmd.CommandText);
                using (SqliteDataReader rdr = cmd.ExecuteReader())
                {
                    //cmd.CommandText = "SELECT " + what + " FROM " + table;
                    //Debug.Log(rdr.FieldCount);
                    while (rdr.Read())
                    {
                        string str = rdr.GetValue(0).ToString();
                        //Debug.Log(counter);
                        result = str;
                        return result;
                    }
                }
            }
        }
        return result;
    }
    //    conn.Open();
    //    //SqliteCommand cmd = new SqliteCommand(conn);
    //    IDbCommand dbcmd = conn.CreateCommand();
    //    IDataReader reader = dbcmd.ExecuteReader();
    //    try
    //    {
    //        //var data = new string[] { };
    //        dbcmd.CommandText = "SELECT " + what + " FROM " + table;
    //        //dbcmd.CommandText = "SELECT "+ what + " FROM "+ table + " WHERE " + who; 

    //        while (reader.Read())
    //        {
    //            string firstName = reader.GetString(0);
    //            string lastName = reader.GetString(1);
    //            Debug.Log("Name: {0} {1}");
    //            Debug.Log(firstName);
    //            Debug.Log(lastName);
    //        }
    //        // clean up
    //        reader.Dispose();
    //        dbcmd.Dispose();
    //        conn.Close();
    //        //return data;
    //    }
    //    catch (SqliteException e)
    //    {
    //        Debug.Log("problema");
    //        reader.Dispose();
    //        dbcmd.Dispose();
    //        conn.Close();
    //        //return ;
    //    }


    void ExportExcel()
    {
        /*
        Dictionary<int, string> dict = new Dictionary<int, string>()
{
    {1,"one"}, {2,"two"}
};

        XElement xElem = new XElement(
                    "items",
                    dict.Select(x => new XElement("item", new XAttribute("id", x.Key), new XAttribute("value", x.Value)))
                 );
        var xml = xElem.ToString();
        */
        string[] Collected = { "a", "b", "c", "d" };
        XDocument doc = new XDocument();
        doc.Add(new XElement("Items"));
        foreach (string item in Collected)
        {
            doc.Element("Items").Add(new XElement("Item", item, new XAttribute("id", "dique"), new XAttribute("ha", "tonces"), new XAttribute("be", "tastsaa")));
        }

        //TextAsset xmlText = Resources.Load("SomeXElements") as TextAsset;
        //XDocument myData = XDocument.Parse(xmlText.text);
        //And this is how you write it
        //myData.Save(Application.dataPath + "/Resources/CollectedXML.xls");
        doc.Save("SomeXElements.xls");

        /*
        XElement xEle = new XElement("XRoot");

        XElement xElem = new XElement("First",
                new XElement("Second"), new XElement("Third"), new XElement("WithInt", 325), new XElement("WithString", "really?"),
                new XElement("WithKids", new XElement("Kid"))
);
        */


        //xEle.Save("SomeXElements.xls");
    }


    void ExportExcel2()

    {
            
    }


    public void AddColumn(string table, string column, string type)
    {
        conn.Open();
        SqliteCommand cmd = new SqliteCommand(conn);

        cmd.CommandText = "ALTER TABLE " + table + " ADD " + column +" "+ type;

        cmd.ExecuteNonQuery();
        conn.Close();
        // ALTER TABLE {tableName} ADD COLUMN COLNew {type};
    }

    public void UpdateSingle(string table, string what, string who)
    {
        //UPDATE company SET address = 'Texas' WHERE id = 6;
        //UpdateSingle("db", "score = '10'", "name = Adrian");
        //UPDATE company SET address = 'Texas' WHERE id = 6;

        conn.Open();
        SqliteCommand cmd = new SqliteCommand(conn);

        //cmd.CommandText = "UPDATE db SET score = '10' WHERE name = fred";
        cmd.CommandText = "UPDATE " + table + " SET " + what + " WHERE " + who;

        cmd.ExecuteNonQuery();
        conn.Close();

    }

    public void UpdateMultiple()
    {
        //UPDATE COMPANY SET ADDRESS = 'Texas', SALARY = 20000.00;
    }

}








//    void Start()
//    {
//        using (var dbcon = new SqliteConnection("URI=file:example.db,version=3"))
//        {
//            dbcon.Open();
//            using (var dbcmd = dbcon.CreateCommand())
//            {
//                dbcmd.CommandText = "CREATE TABLE Example (value REAL);";
//                dbcmd.ExecuteNonQuery();
//            }

//            using (var dbcmd = dbcon.CreateCommand())
//            {
//                dbcmd.CommandText = "INSERT INTO Example (value) VALUES (3.141592653589793);";
//                dbcmd.ExecuteNonQuery();
//            }

//            using (var dbcmd = dbcon.CreateCommand())
//            {
//                dbcmd.CommandText = "SELECT * FROM Example";
//                using (var reader = dbcmd.ExecuteReader())
//                {
//                    if (reader.Read())
//                    {
//                        Debug.Log(reader.GetDataTypeName(0)); // REAL   -- OK
//                        Debug.Log(reader.GetName(0)); // value   -- OK
//                        Debug.Log(reader.GetFloat(0)); // 3.141593   -- seems OK
//                        Debug.Log(reader.GetDouble(0)); // 3.14159265358979   -- OK
//                        Debug.Log(reader.GetValue(0).GetType()); // System.Single   -- Why?
//                        Debug.Log(reader.GetFieldType(0)); // System.Single   -- Why?
//                        Debug.Log((float)reader.GetValue(0)); // 3.141593   -- OK
//                        Debug.Log((double)(float)reader.GetValue(0)); // 3.14159274101257   -- also OK
//                        Debug.Log((double)reader.GetValue(0)); // InvalidCastException   -- Why?
//                    }
//                }
//            }
//        }
//    }
//}