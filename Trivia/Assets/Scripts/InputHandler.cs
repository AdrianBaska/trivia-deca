﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;




public class InputHandler : MonoBehaviour {
    // public variables
    public InputField inputName;
    public InputField inputEmail;
    public DatabaseHandler db;
    public Button playButton;

    //private variables
    private string aInput;
    private string bInput;
    private string registro = "registro";
    private string preguntas = "preguntas";
    //public Text texts; 
    bool found = false;



    // DB setting
    string[] col1 = { "id", "name", "email"};
    string[] type1 = { "INTEGER PRIMARY KEY", "TEXT", "TEXT"};

    string[] col2 = { "id", "pregunta", "res1", "res2", "res3", "res4", "correcta" };
    string[] type2 = { "INTEGER PRIMARY KEY", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "INTEGER DEFAULT 0" };

    private void Awake()
    {
        //playButton.onClick.AddListener(delegate {GameController.control.goToSceneGameOn(); });
        playButton.onClick.AddListener(delegate { Debug.Log("Comienza"); });
    
    }

    void Start () {
        db.CreateTable(registro, col1, type1);
        db.CreateTable(preguntas, col2, type2);


    }
	

	void Update () {
        
        aInput = inputName.text; // get name
        bInput = inputEmail.text; // get email
        //texts.text = db.Selectfirst(table, "name, score", "score DESC", 1); // remove


        //esto lo que hace es autocompletar si reconoce un nombre en la base de datos
        string prueba = db.SelectSingleSTR(registro, "email", "name = '" + aInput + "'"); // look if name exists un DB
        if ((found == false) && (prueba != "")) // if it exists autocomplete email
        {
            inputEmail.text = prueba;
            found = true;
        }
        else if(found == true && prueba=="" ) { // if it doesnt exist write blank
            found = false;
            inputEmail.text = prueba;
        }

    }
    // esta funcion va en el boton iniciar luego de ingresar los datos
    public void SaveNewPlayer()
    {
        //Debug.Log("Guardando en base de datos" + aInput);
        
        db.InsertSingleNoRepeat(registro, "name", aInput);
        if (bInput != "") { 
            db.UpdateSingle(registro, "email = '"+ bInput+"'", "name = '"+aInput+"'");
        }
        //db.SelectAll(table); 
        PlayerPrefs.SetString("Name", aInput);
    }
    public void InsertScore()
    {
        //int newScore = 0;
        //int.TryParse(bInput, out newScore);

        // el nombre se carga al presionar iniciar, pero al finalizar el juego tiene que
        // guardar el score en PlayerPrefs.SetString("Score", suVariableConScore);
        // para que esta funcion lo agarre y lo meta en la base de datos

        string name = PlayerPrefs.GetString("Name");
        int newScore = PlayerPrefs.GetInt("Score");
        int oldScore = db.SelectSingleINT(preguntas, "score", "name = '" + name + "'");
        Debug.Log(oldScore);
        if (oldScore < newScore)
        {
            db.UpdateSingle(preguntas, "score = '" + newScore + "'", "name = '" + name + "'");
            Debug.Log("El score nuevo es mayor y se guardo");
        }
        else {
            Debug.Log("El nuevo score es inferior al anterior");
        }
    }
    public void RetrieveRandomThree()
    {
        
        List<Dictionary<string, string>> dictList =  db.SelectOrderBy(preguntas, "name, score", "score DESC", 5);
        Dictionary<string, string> dict0 = dictList[0];
        Dictionary<string, string> dict1 = dictList[1];
        Dictionary<string, string> dict2 = dictList[2];
        Dictionary<string, string> dict3 = dictList[3];
        Dictionary<string, string> dict4 = dictList[4];

        // estan son las variables que usa, dict["name"] son los nombres, dict[score] son los scores, son tipo string
        Debug.Log(dict0["name"] + " hizo " + dict0["score"] + " puntos");// primer lugar
        Debug.Log(dict1["name"] + " hizo " + dict1["score"] + " puntos");// segundo lugar
        Debug.Log(dict2["name"] + " hizo " + dict2["score"] + " puntos");// tercer lugar
        Debug.Log(dict3["name"] + " hizo " + dict3["score"] + " puntos");// cuarto lugar
        Debug.Log(dict4["name"] + " hizo " + dict4["score"] + " puntos");// quinto lugar

    }
  
}
