﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameHandler : MonoBehaviour
{

    public DoozyUI.UIElement logoIntro;
    public DoozyUI.UIElement logoJuego;
    public DoozyUI.UIElement back;
    public DoozyUI.UIElement respuestas;
    public DoozyUI.UIElement fondoPreguntas;
    public DoozyUI.UIElement preguntas;
    public DoozyUI.UIElement botonInicio;
    public DoozyUI.UIElement exit;
    public DoozyUI.UIElement registro;
    public DoozyUI.UIElement logoRegistro;
    public DoozyUI.UIElement lose;
    public DoozyUI.UIElement winn;
    public DoozyUI.UIElement fondoEspera;
    public DoozyUI.UIElement texto1Intro;
    public DoozyUI.UIElement texto2Intro;
    public DoozyUI.UIElement texto3Intro;


    public AudioSource correctButtonSound;
    public AudioSource wrongButtonSound;
    public AudioSource backgroundMusic;
    public AudioSource winSound;
    public AudioSource loseSound;
    //public AudioClip audioClip;

    //public DoozyUI.UISound sonido;
    //public DoozyUI.UIButton boton;


    //Assets de cambio de color botones de juego
    public Image opcion1;
    public Image opcion2;
    public Image opcion3;
    public Image opcion4;

    public Image sound;
    public Sprite soundOn;
    public Sprite soundOff;

    //Botones de juego para poder deshabilitarlos
    public Button button1;
    public Button button2;
    public Button button3;
    public Button button4;
    public Button buttonInit;

    //Assets de fondo de pregunta
    public Image fondoPregunta;
    public Sprite preg1;
    public Sprite preg2;
    public Sprite preg3;
    public Sprite preg4;
    public Sprite preg5;
    public Sprite preg6;
    public Sprite preg7;

    //Assets de fondo de juego
    public Image fondoJuego;
    public Sprite fondoHashtag;
    public Sprite fondoCanasta;
    public Sprite fondoSimple;



    public TextMeshProUGUI pregunta;
    public TextMeshProUGUI res1;
    public TextMeshProUGUI res2;
    public TextMeshProUGUI res3;
    public TextMeshProUGUI res4;

    Color32 colorWin = new Color32(177, 185, 55, 255);
    Color32 colorLose = new Color32(197, 36, 91, 255);

    List<string> preg;

    List<string> pregunta1 = new List<string>{ "¿Qué idiomas se hablan en Suiza?",
        "a) Alemán, francés, italiano y romanche",
        "b) Alemán, francés y romanche",
        "c) Italiano, francés y alemán",
        "d)	Francés, romanche, italiano.","1","1" };

    List<string> pregunta2 = new List<string>{"¿Cuál es el país más grande del mundo?",
        "a) Brasil",
        "b) China",
        "c) Estados Unidos",
        "d) Rusia", "4","2"  };
    
    List<string> pregunta3 = new List<string>{"¿Copenhague es la capital de..?",
        "a) Dinamarca",
        "b) Noruega",
        "c) Suecia",
        "d) Suiza","1","3"  };

    List<string> pregunta4 = new List<string>{"¿Qué idioma se habla en Holanda? ",
        "a) Alemán",
        "b) Francés",
        "c) Neerlandés",
        "d) Inglés","3","4"  };

    List<string> pregunta5 = new List<string>{"¿Cuál es la capital de Brasil?",
        "a) Rio de Janeiro",
        "b) Brasilia",
        "c) Sao Paulo",
        "d) Porto Alegre","2","5"  };

    List<string> pregunta6 = new List<string>{"¿Cuál es el río más largo del mundo?",
        "a) Amazonas",
        "b) Nilo",
        "c) Mississippi",
        "d) Río Amarillo","1","6"  };

    List<string> pregunta7 = new List<string>{"¿A qué país pertenece la ciudad de Varsovia?",
        "a) Hungría",
        "b) Polonia",
        "c) República Checa",
        "d) Ucrania","2","7"  };
    
    List<string> pregunta8 = new List<string>{"¿Cuántos continentes hay en la Tierra?",
        "a) 8",
        "b) 7",
        "c) 6",
        "d) 5","3","8"  };
    
    List<string> pregunta9 = new List<string>{"¿Cuál es la isla más grande del mar Mediterráneo?",
        "a) Sicilia",
        "b) Chipre",
        "c) Mallorca",
        "d) Ibiza","1","9"  };

    List<string> pregunta10 = new List<string>{"¿Cuántos países hay en el mundo?",
        "a) 196",
        "b) 205",
        "c) 148",
        "d) 194","4","10"  };

    List<string> pregunta11 = new List<string>{"¿Entre cuáles países se encuentra el estrecho de Bering?",
        "a) Canada y Rusia",
        "b) Estados Unidos y Rusia",
        "c) Finlandia y Suecia",
        "d) China y Rusia","2","11"  };
    
    List<string> pregunta12 = new List<string>{"¿A qué continente pertenece la Isla de Madagascar?",
        "a) Asia",
        "b) Oceanía",
        "c) Antártida",
        "d) África","4","12"  };
    
    List<string> pregunta13 = new List<string>{"Trinidad y Tobago son dos islas americanas de habla inglesa situadas en el mar:",
        "a) Caribe",
        "b) Pacífico",
        "c) Negro",
        "d) Atlántico","1","13"  };

    List<string> pregunta14 = new List<string>{"¿El estrecho de Gibraltar separa España de..?",
        "a) Argelia",
        "b) Libia",
        "c) Marruecos",
        "d) Portugal","3","14"  };

    List<string> pregunta15 = new List<string>{"¿Cuál es el país de los 1,000 lagos?",
        "a) Suiza ",
        "b) Brasil",
        "c) Canadá",
        "d) Finlandia", "4","15"  };
    
    List<string> pregunta16 = new List<string>{"¿En qué países están presentes Los Alpes?",
        "a) Austria, Alemania, Francia, Italia, Eslovenia, Suiza.",
        "b) Austria, Alemania, Francia, Italia, Eslovenia, Suiza, Mónaco, Liechtenstein.",
        "c) Austria, Alemania, Suiza.",
        "d) Suiza, Italia, Alemania, Liechtensein.","2","16"  };

    List<string> pregunta17 = new List<string>{"¿Budapest es la capital de..?",
        "a) Suecia",
        "b) Polonia",
        "c) Hungría",
        "d) República Checa","3","17"  };
    
    List<string> pregunta18 = new List<string>{"¿San Petersburgo se encuentra en..?",
        "a) Hungría",
        "b) Polonia",
        "c) Ucrania",
        "d) Rusia","4","18"  };
    
    List<string> pregunta19 = new List<string>{"¿Cuál es la capital de Australia?",
        "a) Perth",
        "b) Sydney",
        "c) Melbourne",
        "d) Canberra","4","19"  };
    
    List<string> pregunta20 = new List<string>{"¿Cuál es el continente más poblado de la tierra?",
        "a) Europa",
        "b) Asia",
        "c) América",
        "d) Africa","2","20"  };
    
    List<string> pregunta21 = new List<string>{"¿Cuál es el país más grande de América del Sur?",
        "a)	Brasil",
        "b)	Argentina",
        "c)	Chile",
        "d)	Bolivia","1","21"  };

    List<string> pregunta22 = new List<string>{"¿Estambul se encuestra en…?",
        "a) Europa",
        "b) Asia",
        "c) Africa",
        "d) Europa y Asia","4","22"  };

    List<string> pregunta23 = new List<string>{"¿En qué ciudad europea se encuentra el Arco del Triunfo?",
        "a)	Roma",
        "b)	Paris",
        "c)	Madrid",
        "d)	Berlin","2","23"  };

    List<string> pregunta24 = new List<string>{"¿Dónde vive la reina de Inglaterra?",
        "a) Palacio de Kensington",
        "b) Palacio de Buckhingham",
        "c) Palacio de Saint Jame’s",
        "d) Palacio de Blenheim","2","24"  };

    List<string> pregunta25 = new List<string>{"¿Cuál de estos países no forma parte de la Unión Europea?",
        "a) Finlandia",
        "b) Suecia",
        "c) Noruega",
        "d) Dinamarca","3","25"  };

    List<string> pregunta26 = new List<string>{"¿Cuáles son los países nórdicos?",
        "a)	Dinamarca, Noruega, Suecia, Finlandia",
        "b)	Finlandia, Noruega y Suecia",
        "c)	Dinamarca, Noruega, Suecia, Finlandia e Islandia",
        "d)	Dinamarca, Suecia, Finlandia","3","26"  };

    List<string> pregunta27 = new List<string>{"¿Cuál es la capital de Argentina?",
        "a) Córdoba",
        "b) Mendoza",
        "c) Buenos Aires",
        "d) Rosario","3","27"  };

    List<string> pregunta28 = new List<string>{"¿Cuál es la capital de Alemania?",
        "a) Frankfurt",
        "b) Munich",
        "c) Berlin",
        "d) Hamburgo","3","28"  };

    List<string> pregunta29 = new List<string>{"¿Praga se encuentra en..?",
        "a) Luxemburgo",
        "b) Suiza",
        "c) República Checa",
        "d) Dinamarca","3","29"  };

    List<string> pregunta30 = new List<string>{"¿Dónde se encuentra la puerta de Brandeburgo?",
        "a) Francia",
        "b) España",
        "c) Italia",
        "d) Alemania","4","30"  };




    List<List<string>> myList = new List<List<string>>();

    private int questionCount = 0;
    private int questionCorrect = 0;
    private IEnumerator nextQ;
    private IEnumerator fail;
    private IEnumerator win;
    private IEnumerator background;

    int posicion1;
    int posicion2;
    int posicion3;

    int fondoSwitch = 0;

    // Start is called before the first frame update
    void Start()
    {   /*
        for (int i = 1; i < 31; i++)
        {
            string pregunta = "pregunta" + i;
            myList.Add(pregunta);

        } */

        myList.Add(pregunta1);
        myList.Add(pregunta2);
        myList.Add(pregunta3);
        myList.Add(pregunta4);
        myList.Add(pregunta5);
        myList.Add(pregunta6);
        myList.Add(pregunta7);
        myList.Add(pregunta8);
        myList.Add(pregunta9);
        myList.Add(pregunta10);
        myList.Add(pregunta11);
        myList.Add(pregunta12);
        myList.Add(pregunta13);
        myList.Add(pregunta14);
        myList.Add(pregunta15);
        myList.Add(pregunta16);
        myList.Add(pregunta17);
        myList.Add(pregunta18);
        myList.Add(pregunta19);
        myList.Add(pregunta20);
        myList.Add(pregunta21);
        myList.Add(pregunta22);
        myList.Add(pregunta23);
        myList.Add(pregunta24);
        myList.Add(pregunta25);
        myList.Add(pregunta26);
        myList.Add(pregunta27);
        myList.Add(pregunta28);
        myList.Add(pregunta29);
        myList.Add(pregunta30);

        //logoIntro.Show(false);

       // texto1Intro.Show(false);
        //texto2Intro.Show(false);
        //texto3Intro.Show(false);
        //fondoSwitch = 1;
        //fondoStandBy();
    }

    // Update is called once per frame
    void Update()
    {
        //boton.customOnClickSound = sonido;
    }

    public void hideAll()
    {
        logoIntro.Hide(false);
        logoJuego.Hide(false);
        fondoPreguntas.Hide(false);
        preguntas.Hide(false);
        respuestas.Hide(false);
        exit.Hide(false);
        botonInicio.Hide(false);
        back.Hide(false);
        registro.Hide(false);
        logoRegistro.Hide(false);
        lose.Hide(false);
        winn.Hide(false);
        texto1Intro.Hide(false);
        texto2Intro.Hide(false);
        texto3Intro.Hide(false);


    }

    public void unload()
    {
        if (logoIntro.isVisible == true)
        {
            logoIntro.Hide(false);                                
        }
        else if(logoIntro.isVisible == false)
        {
            logoIntro.Show(false);
        }
    }

    public void botonIni()
    {
        hideAll();
        StopAllCoroutines();
        fondoGame();
        registro.Show(false);
        back.Show(false);
        logoRegistro.Show(false);

    }

    public void botonRegistro()
    {
        buttonInit.interactable = false;
        hideAll();
        StopAllCoroutines();
        fondoGame();
        questionCount = 0;
        questionCorrect = 0;
        randomQuestion();     
        button1.interactable = true;
        button2.interactable = true;
        button3.interactable = true;
        button4.interactable = true;
        loadQuestion();
        logoJuego.Show(false);
        back.Show(false);
        respuestas.Show(false);
        preguntas.Show(false);
        fondoPreguntas.Show(false);
        buttonInit.interactable = true;

    }

    public void botonBack()
    {
        hideAll();
        StopAllCoroutines();
        //StopCoroutine(nextQ);
        //StopCoroutine(fail);
        fondoJuego.sprite = fondoHashtag;
        logoIntro.Show(false);
        texto1Intro.Show(false);
        texto2Intro.Show(false);
        texto3Intro.Show(false);
        fondoSwitch = 1;
        resetColors();
        //fondoStandBy();
        botonInicio.Show(false);
        exit.Show(false);

    }
    public void loseScreen()
    {
        hideAll();
        //winSound.Play();
        back.Show(false);
        logoIntro.Show(false);
        lose.Show(false);

    }
    public void winScreen()
    {
        hideAll();
        winSound.Play();
        back.Show(false);
        logoIntro.Show(false);
        winn.Show(false);
    }
    public void loadQuestion()
    {
        if (questionCount == 0)
        {
            preg = myList[posicion1];
            showImage();
            pregunta.SetText(preg[0]);
            res1.SetText(preg[1]);
            res2.SetText(preg[2]);
            res3.SetText(preg[3]);
            res4.SetText(preg[4]);
            
        }
        else if (questionCount == 1)
        {
            
            preg = myList[posicion2];
            showImage();
            pregunta.SetText(preg[0]);
            res1.SetText(preg[1]);
            res2.SetText(preg[2]);
            res3.SetText(preg[3]);
            res4.SetText(preg[4]);
            
        }
        else if (questionCount == 2)
        {
            preg = myList[posicion3];
            showImage();
            pregunta.SetText(preg[0]);
            res1.SetText(preg[1]);
            res2.SetText(preg[2]);
            res3.SetText(preg[3]);
            res4.SetText(preg[4]);
        }


    }
    IEnumerator nextStep()
    {
        yield return new WaitForSeconds(2);

        loadQuestion();
        resetColors();
        nextQuestion();
        button1.interactable = true;
        button2.interactable = true;
        button3.interactable = true;
        button4.interactable = true;
    }

    IEnumerator fail1()
    {
        yield return new WaitForSeconds(1);
        loseScreen();


    }

    IEnumerator win1()
    {
        yield return new WaitForSeconds(1);
        winScreen();

    }
    /*
    IEnumerator backgroundChange()
    {
        while (true)
        {
            yield return new WaitForSeconds(10);
            if (fondoSwitch == 0)
            {
                canasta.Hide(false);
                //fondoEspera.Hide(false);  
                fondoJuego.sprite = fondoHashtag;
                logoIntro.Show(false);
                //fondoEspera.Show(false);
                hashtag.Show(false); 
                fondoSwitch = 1;

            }
            else if (fondoSwitch == 1)
            {
                hashtag.Hide(false);
                logoIntro.Hide(false);
                
                //fondoEspera.Hide(false);
                fondoJuego.sprite = fondoCanasta;
                canasta.Show(false);
                //fondoEspera.Show(false);
                fondoSwitch = 0;

            }
        }     
    } */


    public void boton1()
        
    {
        button1.interactable = false;
        button2.interactable = false;
        button3.interactable = false;
        button4.interactable = false;
        questionCount++;

        if (preg[5] == "1")
        {
            correctButtonSound.Play();
            opcion1.color = new Color32(177, 185, 55, 255);
            questionCorrect++;
        }
        else
        {
            wrongButtonSound.Play();
            opcion1.color = new Color32(197, 36, 91, 255);
        }

        if (questionCount == 3)
        {
            if (questionCorrect >= 3)
            {
                win = win1();
                StartCoroutine(win);
            }
            else
            {
                fail = fail1();
                StartCoroutine(fail);
            }

        }
        else
        {
            nextQ = nextStep();
            StartCoroutine(nextQ);
        }


    }
    public void boton2()

    {
        
        button1.interactable = false;
        button2.interactable = false;
        button3.interactable = false;
        button4.interactable = false;
        questionCount++;

        if (preg[5] == "2")
        {
            correctButtonSound.Play();
            opcion2.color = new Color32(177, 185, 55, 255);
            questionCorrect++;
        }
        else
        {
            wrongButtonSound.Play();
            opcion2.color = new Color32(197, 36, 91, 255);
        }

        if (questionCount == 3)
        {
            if (questionCorrect >= 3)
            {
                win = win1();
                StartCoroutine(win);
            }
            else
            {
                fail = fail1();
                StartCoroutine(fail);
            }
        }
        else
        {
            nextQ = nextStep();
            StartCoroutine(nextQ);
        }
    }
    public void boton3()
    {
        button1.interactable = false;
        button2.interactable = false;
        button3.interactable = false;
        button4.interactable = false;
        questionCount++;

        if (preg[5] == "3")
        {
            correctButtonSound.Play();
            opcion3.color = new Color32(177, 185, 55, 255);
            questionCorrect++;
        }
        else
        {
            wrongButtonSound.Play();
            opcion3.color = new Color32(197, 36, 91, 255);
        }

        if (questionCount == 3)
        {
            if (questionCorrect >= 3)
            {
                win = win1();
                StartCoroutine(win);
            }
            else
            {
                fail = fail1();
                StartCoroutine(fail);
            }

        }
        else
        {
            nextQ = nextStep();
            StartCoroutine(nextQ);
        }
    }
    public void boton4()
    {
        button1.interactable = false;
        button2.interactable = false;
        button3.interactable = false;
        button4.interactable = false;
        questionCount++;

        if (preg[5] == "4")
        {
            correctButtonSound.Play();
            opcion4.color = new Color32(177, 185, 55, 255);
            questionCorrect++;   
        }
        else
        {
            wrongButtonSound.Play();
            opcion4.color = new Color32(197, 36, 91, 255);
        }

        if (questionCount == 3)
        {
            if (questionCorrect >= 3)
            {
                win = win1();
                StartCoroutine(win);
            }
            else
            {
                fail = fail1();
                StartCoroutine(fail);
            }

        }
        else
        {
            nextQ = nextStep();
            StartCoroutine(nextQ);
        }

    }
    public void resetColors()
    {
        opcion1.color = new Color32(255, 255, 255, 255); ;
        opcion2.color = new Color32(255, 255, 255, 255); ;
        opcion3.color = new Color32(255, 255, 255, 255); ;
        opcion4.color = new Color32(255, 255, 255, 255); ;
    }
    public void nextQuestion()
    {
        preguntas.Hide(false);
        respuestas.Hide(false);
        preguntas.Show(false);
        respuestas.Show(false);     
    }
    public void randomQuestion()
    {
        posicion1 = Random.Range(0, 30);
        posicion2 = Random.Range(0, 30);
        while (posicion1 == posicion2)
        {
            posicion2 = Random.Range(0, 30);
        }
        posicion3 = Random.Range(0, 30);
        while ((posicion3 == posicion2 ) || (posicion3 == posicion1))
        {
            posicion3 = Random.Range(0, 30);
        }


    }
    public void showImage()
    {
        if (preg[6] == "1")
        {
            fondoPregunta.sprite = preg1;
        }
        if (preg[6] == "2")
        {
            fondoPregunta.sprite = preg2;
        }
        if (preg[6] == "3")
        {
            fondoPregunta.sprite = preg3;
        }
        if (preg[6] == "4")
        {
            fondoPregunta.sprite = preg4;
        }
        if (preg[6] == "5")
        {
            fondoPregunta.sprite = preg5;
        }
        if (preg[6] == "6")
        {
            fondoPregunta.sprite = preg6;
        }
        if (preg[6] == "7")
        {
            fondoPregunta.sprite = preg7;
        }

    }
    /*
    public void fondoStandBy()
    {
        background = backgroundChange();
        StartCoroutine(background);
    } */

    public void fondoGame()
    {
        fondoEspera.Hide(false);
        fondoJuego.sprite = fondoSimple;
        fondoEspera.Show(false);
    }

    public void buttonExit()
    {
        Application.Quit();
    }

    bool isMute = false;

    public void Mute()
    {
        if (isMute == true)
        {
            backgroundMusic.volume = 0.148f;
            sound.sprite = soundOn;
            isMute = false;
        }
        else if (isMute == false)
        {
            backgroundMusic.volume = 0;
            sound.sprite = soundOff;
            isMute = true;
        }
    }
}
